var clientesObtenidos;
var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

function getClientes(){

  //var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request=new XMLHttpRequest(); //libreria para hacer peticiones a apis

  request.onreadystatechange = function(){
    if (this.readyState == 4  //valida que ya haya terminado de cargar la aplicación
          && this.status == 200) {

      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();

    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONClientes.value[0].ProductName);

  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("tableClientes");
  var tbody = document.createElement("tbodyClientes");

  tabla.classList.add("table");
  tabla.classList.add("table-stripped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaContacto = document.createElement("td");
    columnaContacto.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if (JSONClientes.value[i].Country == "UK") {
        imgBandera.src = rutaBandera + "United-Kingdom.png";
    }
    else{
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }

    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaContacto);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);

  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
